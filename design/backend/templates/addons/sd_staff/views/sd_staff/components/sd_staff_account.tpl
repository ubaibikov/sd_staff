{include file="common/subheader.tpl" title=__("sd_staff_staff_member_information")}

<div class="control-group">
    <label for="sd_staff_first_name" class="control-label cm-required ">{__("sd_staff_first_name")}:</label>
    <div class="controls">
            <input type="text" id="sd_staff_first_name" name="staff_data[first_name]" class="input-large" size="32" maxlength="128" value="{$staff_data.first_name}"/>
    </div>
</div>

<div class="control-group">
    <label for="sd_staff_last_name" class="control-label cm-required ">{__("sd_staff_last_name")}:</label>
    <div class="controls">
            <input type="text" id="sd_staff_last_name" name="staff_data[last_name]" class="input-large" size="32" maxlength="128" value="{$staff_data.last_name}" />
    </div>
</div>

<div class="control-group">
    <label for="sd_staff_function" class="control-label cm-required">{__("sd_staff_function")}:</label>
    <div class="controls">
            <input type="text" id="sd_staff_function" name="staff_data[function]" class="input-large" size="32" maxlength="128" value="{$staff_data.function}" />
    </div>
</div>


<div class="control-group">
    <label for="sd_staff_email" class="control-label  cm-required cm-email">{__("sd_staff_email")}:</label>
    <div class="controls">
            <input type="text" id="sd_staff_email" name="staff_data[email]" class="input-large" size="32" maxlength="128"  value="{$staff_data.email}"/>
    </div>
</div>

<div class="control-group">
    <label for="sd_staff_pos" class="control-label">{__("sd_staff_pos")}:</label>
    <div class="controls">
            <input type="text" id="sd_staff_pos" name="staff_data[point_of_sale]" class="input-large" size="32" maxlength="128" value="{$staff_data.point_of_sale}"/>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="sd_staff_description">{__("sd_staff_description")}:</label>
    <div class="controls">
          <textarea id="sd_staff_description"
                                      name="staff_data[description]"
                                      cols="55"
                                      rows="8"
                                      class="cm-wysiwyg input-large"
        >{$staff_data.description}</textarea>
    </div>
</div>

<div class="control-group">
    <label class="control-label">{__("sd_staff_photo")}:</label>
    <div class="controls">
        {include file="common/attach_images.tpl"
            image_name="staff_main"
            image_object_type="staff" 
            image_pair=$staff_data.main_pair
            no_detailed=true 
            hide_titles=true 
            image_object_id=$staff_data.id}
    </div>
</div>