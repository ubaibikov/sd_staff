{$c_url = $config.current_url|fn_query_remove:"sort_by":"sort_order"}
{$c_icon = "<i class=\"icon-`$search.sort_order_rev`\"></i>"}
{$c_dummy = "<i class=\"icon-dummy\"></i>"}
{$rev = $smarty.request.content_id|default:"pagination_contents_staff"}

<table class="table table-middle table-responsive">
    <thead>
        <tr>
                <th class="mobile-hide"><a href="{"`$c_url`&sort_by=point_of_sale&sort_order=`$search.sort_order_rev`"|fn_url}"  data-ca-target-id={$rev}>{__("sd_staff_pos")} {if $search.sort_by == "point_of_sale"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
                <th class="mobile-hide">{__("sd_staff_name")}</th>
                <th width="10%" class="mobile-hide">&nbsp;</th>
                <th class="mobile-hide right"> <a href="{"`$c_url`&sort_by=status&sort_order=`$search.sort_order_rev`"|fn_url}"  data-ca-target-id={$rev}> {__("sd_staff_status")}{if $search.sort_by == "status"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        </tr>
    </thead>
        <tbody>
        {foreach $staff_accounts as $staff_account}
                <tr class="cm-row-status-{$staff_account.status|lower}">
                        {$allow_save = $staff_account|fn_allow_save_object:"staff"}
                        {if $allow_save}
                                {$no_hide_input = "cm-no-hide-input"}
                        {else}
                                {$no_hide_input = ""}
                        {/if}

                        <td class="{$no_hide_input}">{$staff_account.point_of_sale}</td>
                        <td class="{$no_hide_input}"><a href="{"sd_staff.update&staff_id=`$staff_account.id`"|fn_url}">{$staff_account.first_name} {$staff_account.last_name}</a></td>
                        <td width="10%" class="mobile-hide {$no_hide_input}" data-th="{__("tools")}">
                        {capture name="tools_list"}
                                <li>{btn type="list" text=__("edit") href="sd_staff.update&staff_id=`$staff_account.id`"}</li>                                               
                                {if $allow_save}
                                        <li>{btn type="list" text=__("delete") class="cm-confirm" href="sd_staff.delete?staff_id=`$staff_account.id`" method="POST"}</li>
                                {/if}
                        {/capture}
                        <div class="hidden-tools">
                                {dropdown content=$smarty.capture.tools_list}
                        </div>
                        </td>
                        <td width="10%"class="nowrap right" data-th="{__("status")}">
                                {include file="common/select_popup.tpl" 
                                        id=$staff_account.id 
                                        status=$staff_account.status 
                                        hidden=false 
                                        object_id_name="id" 
                                        table="staff" 
                                        popup_additional_class="`$no_hide_input` dropleft" 
                                        hide_for_vendor=false}
                        </td>
                </tr>
        {/foreach}
        </tbody>
</table>