{capture name="mainbox"}

<form action="{""|fn_url}" method="post" class="cm-hide-inputs">
<input type="hidden" name="fake" value="1" />
{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id="pagination_contents_staff"}
<div class="table-responsive-wrapper">
        {if $staff_accounts}
        <div class="items-container multi-level">
                {include file="addons/sd_staff/views/sd_staff/components/sd_staff_accounts_list.tpl"}
        </div>
        {else}
                <p class="no-items">{__("no_data")}</p>
        {/if}
</div>
{include file="common/pagination.tpl" div_id="pagination_contents_staff"}


{capture name="adv_buttons"}
        <a class="btn cm-tooltip" href="{"sd_staff.add"|fn_url}" title="{__("sd_staff_add_member")}"><i class="icon-plus"></i></a>
{/capture}

</form>
{/capture}

{include 
    file="common/mainbox.tpl"
    title=__("sd_staff")
    content=$smarty.capture.mainbox
    buttons=$smarty.capture.buttons
    sidebar=$smarty.capture.sidebar
    adv_buttons=$smarty.capture.adv_buttons
}
