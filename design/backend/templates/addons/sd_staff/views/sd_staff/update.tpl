{if $staff_data}
    {$id = $staff_data.id}
{else}
    {$id = 0}
{/if}
<form name="staff_form" enctype="multipart/form-data" action="{""|fn_url}" method="post" class="form-horizontal form-edit form-table admin-content-wrapper">
{capture name="mainbox"}

        {capture name="tabsbox"}
                <input type="hidden" name="staff_id" value="{$id}" />
                <div id="content_general">
                         {include file="addons/sd_staff/views/sd_staff/components/sd_staff_account.tpl"}
                </div>
        {/capture}
        
        {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox group_name=$runtime.controller  track=true}
{/capture}
{capture name="adv_buttons"}
        {if $id}
                {include file="buttons/save_changes.tpl" but_meta="dropdown-toggle" but_role="submit-link" but_name="dispatch[sd_staff.`$runtime.mode`]" but_target_form="profile_form" save=$id}
        {else}
                {include file="buttons/button.tpl" but_text=__("create") but_meta="dropdown-toggle" but_role="submit-link" but_name="dispatch[sd_staff.`$runtime.mode`]" but_target_form="profile_form" save=$id}
        {/if}
{/capture}

{include 
    file="common/mainbox.tpl"
    title=__("sd_staff")
    content=$smarty.capture.mainbox
    buttons=$smarty.capture.buttons
    sidebar=$smarty.capture.sidebar
    adv_buttons=$smarty.capture.adv_buttons
}
