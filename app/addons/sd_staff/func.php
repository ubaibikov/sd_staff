<?php

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

/**
 * Update/Create Staff account 
 *
 * @param int $staff_id  Staff Account identifier
 * @param array $staff_data Update or create staff account data
 *
 * @return boolean
 */
function fn_update_sd_staff_account_data(int $staff_id, array $staff_data): bool
{
    //create staff account
    if ($staff_id == 0 && !empty($staff_data)) {

        $staff_email = $staff_data['email'];

        //logical because email is always unique
        $staff_account_email_exists = db_get_field('SELECT id FROM ?:staff WHERE email = ?s', $staff_email);

        if (empty($staff_account_email_exists)) {
            $create_staff_account = db_query('INSERT INTO ?:staff ?e', $staff_data);
            fn_update_sd_staff_account_image($create_staff_account);

            fn_set_notification('N', __('notice'), __('sd_staff_account_created'));
            return true;
        }

        fn_set_notification('W', __('warning'), __('sd_staff_account_exists'));
        return false;
    }

    //update staff account 
    if (!empty($staff_data)) {
        db_query('UPDATE ?:staff SET ?u WHERE id = ?i', $staff_data, $staff_id);
        fn_update_sd_staff_account_image($staff_id);

        return true;
    }
}

/**
 * Get Staff accounts list 
 *
 * @param array $params $_REQUEST or params for get staff accounts
 * @param int $items_per_page Count item per page default 0 
 *
 * @return array
 */
function fn_get_sd_staff_accounts(array $params, int $items_per_page = 0): array
{
    $paginate_default_params = [
        'page' => 1,
        'items_per_page' => $items_per_page,
        'total_items' => 10,
        'sort_by' => 'status'
    ];

    $sortings = [
        'point_of_sale' => 'point_of_sale',
        'status' => 'status',
    ];

    $params = array_merge($paginate_default_params, $params);

    $params['total_items'] = db_get_field('SELECT COUNT(?:staff.id) FROM ?:staff WHERE ?i = ?i', 1, 1);

    $sorting = db_sort($params, $sortings, 'status', 'desc');
    $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);

    $staff_accounts_list = db_get_array("SELECT * FROM ?:staff $sorting $limit");
    return [$staff_accounts_list, $params];
}

/**
 * Get Staff account 
 *
 * @param int $staff_id  Staff Account identifier
 *
 * @return array
 */
function fn_get_sd_staff_account(int $staff_id): array
{
    $staff_data =  db_get_row('SELECT * FROM ?:staff WHERE id = ?i', $staff_id);

    if (!empty($staff_data)) {
        $staff_data['main_pair'] = fn_get_sd_staff_account_image_main_pair($staff_id);
    }

    return $staff_data;
}

/**
 * Update/Create Staff account image  
 *
 * @param int $staff_id  Staff Account identifier
 * @param string $lang_code Cart lang code default CART_LANGUAGE
 * 
 * @return array
 */
function fn_update_sd_staff_account_image(int $staff_id, $lang_code = CART_LANGUAGE)
{
    $delete_last_image = fn_delete_image_pairs($staff_id, 'staff');
    return fn_attach_image_pairs('staff_main', 'staff', $staff_id, $lang_code);
}

/**
 * Get Staff account image pairs 
 *
 * @param int $staff_id  Staff Account identifier
 * @param string $lang_code Cart lang code default CART_LANGUAGE
 * 
 * @return array
 */
function fn_get_sd_staff_account_image_main_pair(int $staff_id, $lang_code = CART_LANGUAGE)
{
    return fn_get_image_pairs($staff_id, 'staff', 'M', true, true, $lang_code);
}
/**
 * Delete Staff account  
 *
 * @param int $staff_id  Staff Account identifier
 * @param string $lang_code Cart lang code default CART_LANGUAGE
 * 
 */
function fn_delete_sd_staff_account(int $staff_id, $lang_code = CART_LANGUAGE)
{
    $delete_account = db_query('DELETE FROM ?:staff WHERE id = ?s', $staff_id);

    fn_delete_sd_staff_account_image_pairs($staff_id);
}

/**
 * Delete Staff account image pairs 
 *
 * @param int $staff_id  Staff Account identifier
 * 
 * @return bool
 */
function fn_delete_sd_staff_account_image_pairs(int $staff_id)
{
    return fn_delete_image_pairs($staff_id, 'staff');
}
