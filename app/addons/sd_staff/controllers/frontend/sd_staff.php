<?php

use Tygh\Registry;
use Tygh\Tygh;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}


if ($mode == 'manage') {
    $params = $_REQUEST;
    list($staff_accounts_list, $search) = fn_get_sd_staff_accounts($params, Registry::get('settings.Appearance.admin_elements_per_page'));
    if (!empty($staff_accounts_list)) {
        fn_add_breadcrumb(__('sd_staff'));
        Tygh::$app['view']->assign([
            'staff_accounts' => $staff_accounts_list,
            'search' => $search,
        ]);
    }
} elseif ($mode == 'view') {
    if (!empty($staff_id = $_REQUEST['staff_id'])) {
        $staff_data = fn_get_sd_staff_account($staff_id);
        if (!empty($staff_data)) {
            fn_add_breadcrumb(__('sd_staff'), "sd_staff.manage");
            fn_add_breadcrumb(__('sd_staff_staff_member_information'), "sd_staff.view?staff_id=$staff_id");
            Tygh::$app['view']->assign(['staff_data' => $staff_data]);
        } else {
            return [CONTROLLER_STATUS_NO_PAGE];
        }
    }
}
