<?php

use Tygh\Registry;
use Tygh\Tygh;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    //delete account
    if ($mode == 'delete') {
        if (!empty($_REQUEST['staff_id'])) {
            fn_delete_sd_staff_account($_REQUEST['staff_id']);
            $suffix = '.manage';
        }
    }
    // create/update account
    if ($mode == 'update' || $mode == 'add') {
        $staff_id = $_REQUEST['staff_id'];
        $staff_data = $_REQUEST['staff_data'];

        if (!empty($staff_data)) {
            $result = fn_update_sd_staff_account_data($staff_id, $staff_data);
            $suffix = '.add';
        }
    }

    return [CONTROLLER_STATUS_OK, 'sd_staff' . $suffix];
}

if ($mode == 'manage') {
    $params = $_REQUEST;
    list($staff_accounts_list, $search) = fn_get_sd_staff_accounts($params, Registry::get('settings.Appearance.admin_elements_per_page'));;

    Tygh::$app['view']->assign([
        'staff_accounts' => $staff_accounts_list,
        'search' => $search,
    ]);
} elseif ($mode == 'update') {
    if (!empty($staff_id = $_REQUEST['staff_id'])) {
        $staff_data = fn_get_sd_staff_account($staff_id);
        Tygh::$app['view']->assign(['staff_data' => $staff_data]);
    }
}
