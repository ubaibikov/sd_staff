<?php

$schema['central']['website']['items']['sd_staff'] = [
    'attrs' => [
        'class' => 'is-addon'
    ],
    'href' => 'sd_staff.manage',
    'position' => 100
];

return $schema;
