<div>
    <div class="ty-control-group ty-profile-field__item ty-first-name">
        <label for="sd_staff_email" class="ty-control-group__title cm-trim">{__("sd_staff_first_name")}</label>
        <span class="input-large" id="sd_staff_email">{$staff_data.first_name}</span>
    </div>

    <div class="ty-control-group ty-profile-field__item ty-first-name">
        <label for="sd_staff_last_name" class="ty-control-group__title ">{__("sd_staff_last_name")}</label>
        <span class="input-large" id="sd_staff_last_name">{$staff_data.last_name}</span>
    </div>

    <div class="ty-control-group ty-profile-field__item ty-first-name">
        <label for="sd_staff_email" class="ty-control-group__title ">{__("sd_staff_email")}</label>
        <span class="input-large" id="sd_staff_email">{$staff_data.email}</span>
    </div>
    <div class="ty-control-group ty-profile-field__item ty-first-name">
        <label for="sd_staff_pos" class="ty-control-group__title ">{__("sd_staff_pos")}</label>
        <span class="input-large" id="sd_staff_pos">{$staff_data.point_of_sale}</span>
    </div>

    <div class="ty-control-group ty-profile-field__item ty-first-name">
        <label for="sd_staff_function" class="ty-control-group__title ">{__("sd_staff_function")}</label>
        <span class="input-large" id="sd_staff_function">{$staff_data.function}</span>
    </div>
    {if $staff_data.description}
        <div class="ty-control-group">
            <label for="sd_staff_description" class="ty-control-group__title ">{__("sd_staff_description")}</label>
            <span class="input-large" id="sd_staff_description">{$staff_data.description}</span>
        </div>
    {/if}
    {if $staff_data.main_pair}
        <div class="ty-control-group">
            <label for="sd_staff_photo" class="ty-control-group__title ">{__("sd_staff_photo")}</label>
            {include file="common/image.tpl"
                images=$staff_data.main_pair 
                image_height="200" 
                object_id="sd_staff_photo"}
        </div>
    {/if}
</div>