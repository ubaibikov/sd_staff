<form action="{"store_locator.search"|fn_url}" id="sd_staff_form">

    {include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id="pagination_contents_staff"}
        <div id="sd_staff_controls">
            {if $staff_accounts}
                    {include file="addons/sd_staff/views/sd_staff/components/sd_staff_accounts_list.tpl"}
                {else}
                    <p class="ty-no-items">{__("no_data")}</p>
            {/if}
        </div>
    {include file="common/pagination.tpl" div_id="pagination_contents_staff"}
    
</form>
{capture name="mainbox_title"}{__("sd_staff")}{/capture}
