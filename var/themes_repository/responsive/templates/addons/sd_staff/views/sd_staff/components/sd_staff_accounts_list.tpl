 <div class="ty-variations-list__wrapper">
    <table class="ty-variations-list ty-table ty-table--sorter" data-ca-sortable="true" data-ca-sort-list="[[1, 0]]">
        <thead>
            <tr>
                <th class="ty-variations-list__title ty-left cm-tablesorter" data-ca-sortable-column="false">{__("sd_staff_photo")}</th>
                <th class="ty-variations-list__title ty-left cm-tablesorter" data-ca-sortable-column="false">{__("sd_staff_name")}</th>
                <th class="ty-variations-list__title ty-left cm-tablesorter" data-ca-sortable-column="false">{__("sd_staff_function")}</th>
            </tr>
        </thead>
        <tbody>
            {foreach  $staff_accounts as $staff_account}
                    <tr class="ty-variations-list__item">                        
                        {$image_pair = fn_get_sd_staff_account_image_main_pair($staff_account.id)}

                        <td class="ty-variations-list__product-elem ty-variations-list__image">
                            {if $image_pair}
                                {include file="common/image.tpl"
                                    images= $image_pair
                                    image_height="60" 
                                    object_id="sd_staff_photo_`$staff_account.id`" 
                                    class="circle-staff-account__image"}
                            {/if}
                        </th>
                        <td class="ty-variations-list__product-elem ty-variations-list__image"><a href="{"sd_staff.view&staff_id=`$staff_account.id`"|fn_url}">{$staff_account.first_name} {$staff_account.last_name}</a></td>
                        <td class="ty-variations-list__product-elem ty-variations-list__image">{$staff_account.function}</td>
                    </tr>
            {/foreach}
        </tbody>
    </table>
</div>