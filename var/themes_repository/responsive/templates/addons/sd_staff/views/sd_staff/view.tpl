{if $staff_data}
    <form action="{"store_locator.search"|fn_url}" id="sd_staff_member_form" class="form-horizontal form-edit form-table cm-hide-inputs">
        <div id="sd_staff_controls">
            {include file="addons/sd_staff/views/sd_staff/components/sd_staff_account.tpl"}
        </div>
    </form>
    {capture name="mainbox_title"}{__("sd_staff_staff_member_information")}{/capture}
{/if}